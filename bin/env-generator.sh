#!/bin/bash

cd ..

if [ -f .env ]; then
    echo ".env file already exists in the project root. Skipping..."
else
    cp .env.example .env

    app_key="base64:$(openssl rand -base64 32)"
    jwt_secret=$(openssl rand -base64 32)
    db_password=$(openssl rand -base64 32)

    sed -i "s#^APP_KEY=.*#APP_KEY=$app_key#" .env
    sed -i "s#^JWT_SECRET=.*#JWT_SECRET=$jwt_secret#" .env
    sed -i "s#^DB_PASSWORD=.*#DB_PASSWORD=$db_password#" .env
fi