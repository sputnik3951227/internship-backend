<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FavoriteSpotController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\RecommendedSpotController;
use App\Http\Controllers\ScoreController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersPictureController;
use App\Http\Controllers\UsersRoleController;
use App\Http\Controllers\VacationSpotContoller;
use App\Http\Controllers\VacationSpotsCategoryController;
use App\Http\Controllers\VacationSpotsPictureController;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')->name('auth')->middleware('api')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::get('me', [AuthController::class, 'getCurrentUser']);
});

Orion::resource('users', UserController::class);

Orion::resource('spots', VacationSpotContoller::class);

Orion::resource('categories', VacationSpotsCategoryController::class);

Orion::resource('users-pictures', UsersPictureController::class);

Orion::resource('favorites', FavoriteSpotController::class);

Orion::resource('users-roles', UsersRoleController::class);

Orion::resource('recommended-spots', RecommendedSpotController::class);

Orion::resource('scores', ScoreController::class);

Orion::resource('spots-pictures', VacationSpotsPictureController::class);

Orion::resource('notifications', NotificationController::class);
