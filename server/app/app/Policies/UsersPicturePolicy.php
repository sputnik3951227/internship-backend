<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UsersPicture;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UsersPicturePolicy
{
    use HandlesAuthorization;

    public function before(User $user, string $ability): bool|null
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isBlocked()) {
            return false;
        }

        return null;
    }

    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, UsersPicture $usersPicture): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, UsersPicture $usersPicture): Response
    {
        return $this->deny();
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, UsersPicture $usersPicture): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, UsersPicture $usersPicture): Response
    {
        return $this->allow();
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, UsersPicture $usersPicture): Response
    {
        return $this->allow();
    }
}
