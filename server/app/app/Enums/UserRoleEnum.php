<?php

namespace App\Enums;

enum UserRoleEnum: string
{
    use ValuesTrait;

    case Admin = 'admin';
    case User = 'user';
    case Blocked = 'blocked';
}
