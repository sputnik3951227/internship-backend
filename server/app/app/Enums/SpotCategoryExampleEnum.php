<?php

namespace App\Enums;

enum SpotCategoryExampleEnum: string
{
    use ValuesTrait;

    case Forest = 'forest';
    case Park = 'park';
    case Beach = 'beach';
    case Mountain = 'mountain';
}
