<?php

namespace App\Enums;

enum CountryEnum: string
{
    use ValuesTrait;

    case France = 'France';
    case Spain = 'Spain';
    case Italy = 'Italy';
    case USA = 'United States';
    case China = 'China';
    case UK = 'United Kingdom';
    case Germany = 'Germany';
    case Mexico = 'Mexico';
    case Thailand = 'Thailand';
    case Japan = 'Japan';
    case Russia = 'Russia';
}
