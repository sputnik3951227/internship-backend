<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScoreRequest;
use App\Http\Resources\ScoreResource;
use App\Models\Score;
use App\Policies\ScorePolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class ScoreController extends Controller
{
    protected $model = Score::class;

    protected $resource = ScoreResource::class;

    protected $request = ScoreRequest::class;

    protected $policy = ScorePolicy::class;

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if (!$user->isAdmin()) {
            $query->where('user_id', $user->id);
        }

        return $query;
    }
}
