<?php

namespace App\Http\Controllers;

use App\Http\Requests\VacationSpotsPictureRequest;
use App\Http\Resources\VacationSpotsPictureResource;
use App\Models\VacationSpotsPicture;
use App\Policies\VacationSpotsPicturePolicy;
use Orion\Http\Controllers\Controller;

class VacationSpotsPictureController extends Controller
{
    protected $model = VacationSpotsPicture::class;

    protected $request = VacationSpotsPictureRequest::class;

    protected $resource = VacationSpotsPictureResource::class;

    protected $policy = VacationSpotsPicturePolicy::class;
}
