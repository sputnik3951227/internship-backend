<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecommendedSpotRequest;
use App\Http\Resources\RecommendedSpotResource;
use App\Models\RecommendedSpot;
use App\Policies\RecommendedSpotPolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class RecommendedSpotController extends Controller
{
    protected $model = RecommendedSpot::class;

    protected $resource = RecommendedSpotResource::class;

    protected $request = RecommendedSpotRequest::class;

    protected $policy = RecommendedSpotPolicy::class;

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if (!$user->isAdmin()) {
            $query->where('user_id', $user->id);
        }

        return $query;
    }
}
