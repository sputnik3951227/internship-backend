<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersPictureRequest;
use App\Http\Resources\UsersPictureResourse;
use App\Models\UsersPicture;
use App\Policies\UsersPicturePolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class UsersPictureController extends Controller
{
    protected $model = UsersPicture::class;

    protected $request = UsersPictureRequest::class;

    protected $resource = UsersPictureResourse::class;

    protected $policy = UsersPicturePolicy::class;

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if (!$user->isAdmin()) {
            $query->where('user_id', $user->id);
        }

        return $query;
    }
}
