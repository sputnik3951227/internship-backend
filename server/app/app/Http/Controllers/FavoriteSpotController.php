<?php

namespace App\Http\Controllers;

use App\Http\Requests\FavoriteSpotRequest;
use App\Http\Resources\FavoriteSpotResourse;
use App\Models\FavoriteSpot;
use App\Policies\FavoriteSpotPolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class FavoriteSpotController extends Controller
{
    protected $model = FavoriteSpot::class;

    protected $request = FavoriteSpotRequest::class;

    protected $resource = FavoriteSpotResourse::class;

    protected $policy = FavoriteSpotPolicy::class;

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if (!$user->isAdmin()) {
            $query->where('user_id', $user->id);
        }

        return $query;
    }
}
