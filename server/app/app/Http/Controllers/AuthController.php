<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\TokenResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Unauthorized');
        }

        $tokenResource = new TokenResource($token);

        return $tokenResource;
    }

    public function getCurrentUser(Request $request)
    {
        if (!auth()->check()) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'You are not logged in to your account');
        }

        return new UserResource(auth()->user());
    }
}
