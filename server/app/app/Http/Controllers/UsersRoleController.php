<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRoleRequest;
use App\Http\Resources\UsersRoleResource;
use App\Models\UsersRole;
use App\Policies\UsersRolePolicy;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class UsersRoleController extends Controller
{
    protected $model = UsersRole::class;

    protected $request = UsersRoleRequest::class;

    protected $resource = UsersRoleResource::class;

    protected $policy = UsersRolePolicy::class;

    protected function buildFetchQuery(Request $request, array $requestedRelations): Builder
    {
        $query = parent::buildFetchQuery($request, $requestedRelations);

        $user = auth()->user();
        if (!$user->isAdmin()) {
            $query->where('user_id', $user->id);
        }

        return $query;
    }
}
