<?php

namespace App\Http\Controllers;

use App\Http\Requests\VacationSpotsCategoryRequest;
use App\Models\VacationSpotsCategory;
use App\Policies\VacationSpotsCategoryPolicy;
use Orion\Http\Controllers\Controller;

class VacationSpotsCategoryController extends Controller
{
    protected $model = VacationSpotsCategory::class;

    protected $request = VacationSpotsCategoryRequest::class;

    protected $policy = VacationSpotsCategoryPolicy::class;
}
