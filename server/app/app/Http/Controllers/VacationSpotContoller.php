<?php

namespace App\Http\Controllers;

use App\Http\Requests\VacationSpotRequest;
use App\Http\Resources\VacationSpotResource;
use App\Models\VacationSpot;
use App\Policies\VacationSpotPolicy;
use Orion\Http\Controllers\Controller;

class VacationSpotContoller extends Controller
{
    protected $model = VacationSpot::class;

    protected $request = VacationSpotRequest::class;

    protected $policy = VacationSpotPolicy::class;

    protected $resource = VacationSpotResource::class;
}
