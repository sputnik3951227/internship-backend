<?php

namespace App\Http\Requests;

use App\Enums\CountryEnum;
use Illuminate\Validation\Rules\Enum;
use Orion\Http\Requests\Request;

class VacationSpotRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => 'required|string|min:1|max:128|unique:vacation_spots',
            'description' => 'string|min:1|max:5000',
            'latitude' => 'required|string|min:2|max:16',
            'longitude' => 'required|string|min:2|max:16',
            'average_score' => 'prohibited',
            'spot_category_id' => 'required|integer|exists:vacation_spots_categories,id',
            'country' => ['required', 'string', new Enum(CountryEnum::class)],
        ];
    }

    public function updateRules(): array
    {
        return [
            'name' => 'string|min:1|max:128|unique:vacation_spots',
            'description' => 'string|min:1|max:5000',
            'latitude' => 'string|min:2|max:16',
            'longitude' => 'string|min:2|max:16',
            'average_score' => 'prohibited',
            'spot_category_id' => 'integer|exists:vacation_spots_categories,id',
            'country' => ['string', new Enum(CountryEnum::class)],
        ];
    }
}
