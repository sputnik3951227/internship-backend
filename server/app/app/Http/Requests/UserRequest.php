<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class UserRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'email' => 'required|string|min:1|max:255|email|unique:users',
            'password' => 'required|string|min:6|max:32',
            'repeated_password' => 'required|same:password',
            'username' => 'prohibited',
            'first_name' => 'prohibited',
            'last_name' => 'prohibited',
        ];
    }

    public function updateRules(): array
    {
        return [
            'email' => 'string|min:1|max:255|email|unique:users',
            'username' => 'required|string|min:1|max:64',
            'first_name' => 'required|string|min:1|max:50',
            'last_name' => 'required|string|min:1|max:50',
        ];
    }
}
