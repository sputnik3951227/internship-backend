<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class ScoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'value' => 'required|integer|between:1,5',
            'comment' => 'string|min:1|max:5000',
            'user_id' => 'prohibited',
            'vacation_spot_id' => 'required|integer|exists:vacation_spots,id',
        ];
    }

    public function updateRules(): array
    {
        return [
            'value' => 'integer|between:1,5',
            'comment' => 'string|min:1|max:5000',
            'user_id' => 'prohibited',
            'vacation_spot_id' => 'prohibited',
        ];
    }
}
