<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class UsersPictureRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'picture_link' => 'required|string|min:1|max:255',
            'is_profile_picture' => 'required|boolean',
        ];
    }

    public function updateRules(): array
    {
        return [
            'picture_link' => 'string|min:1|max:255',
            'is_profile_picture' => 'boolean',
        ];
    }
}
