<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class FavoriteSpotRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'planned_time' => 'date_format:d.m.Y',
            'vacation_spot_id' => 'required|integer|exists:vacation_spots,id',
        ];
    }

    public function updateRules(): array
    {
        return [
            'planned_time' => 'date_format:d.m.Y',
            'vacation_spot_id' => 'integer|exists:vacation_spots,id',
        ];
    }
}
