<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class VacationSpotsPictureRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'picture_link' => 'required|string|min:1|max:255',
            'vacation_spot_id' => 'required|integer|exists:vacation_spots,id',
        ];
    }

    public function updateRules(): array
    {
        return [
            'picture_link' => 'string|min:1|max:255',
            'vacation_spot_id' => 'integer|exists:vacation_spots,id',
        ];
    }
}
