<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class VacationSpotsCategoryRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => 'required|string|min:1|max:32|unique:vacation_spots_categories',
        ];
    }

    public function updateRules(): array
    {
        return [
            'name' => 'required|string|min:1|max:32|unique:vacation_spots_categories',
        ];
    }
}
