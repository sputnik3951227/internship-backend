<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class RecommendedSpotRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'user_id' => 'required|integer|exists:users,id',
            'vacation_spot_id' => 'required|integer|exists:vacation_spots,id',
        ];
    }
}
