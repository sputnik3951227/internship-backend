<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class NotificationRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'message' => 'prohibited',
            'user_id' => 'prohibited',
        ];
    }
}
