<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Orion\Http\Resources\Resource;

class UsersPictureResourse extends Resource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'picture_link' => $this->picture_link,
            'user_id' => $this->user_id,
            'is_profile_picture' => $this->is_profile_picture,
        ];
    }
}
