<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FavoriteSpotResourse extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'planned_time' => date('d.m.Y', strtotime($this->planned_time)),
            'user_id' => $this->user_id,
            'vacation_spot_id' => $this->vacation_spot_id,
        ];
    }
}
