<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Table: scores.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property int                 $value
 * @property string|null         $comment
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int                 $vacation_spot_id
 * @property int                 $user_id
 *
 * === Relationships ===
 * @property VacationSpot|null $vacationSpot
 * @property User|null         $user
 */
class Score extends Model
{
    use HasFactory;
    use AssociateUserTrait;

    protected $fillable = [
        'value',
        'comment',
        'user_id',
        'vacation_spot_id',
    ];

    public function vacationSpot(): BelongsTo
    {
        return $this->belongsTo(VacationSpot::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function cancelIfAlreadyExist(): void
    {
        if (
            self::query()
                ->where('user_id', $this->user_id)
                ->where('vacation_spot_id', $this->vacation_spot_id)
                ->exists()
        ) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "You've already rated this spot");
        }
    }

    public function cancelIfWasntFavorite(): void
    {
        if (
            FavoriteSpot::query()
                ->where('user_id', $this->user_id)
                ->where('vacation_spot_id', $this->vacation_spot_id)
                ->doesntExist()
        ) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'You can only rate a spot from your favorite list');
        }
    }

    private function calculateAverageScore(): void
    {
        $vacationSpot = $this->vacationSpot;
        $vacationSpot->average_score = self::where('vacation_spot_id', $this->vacation_spot_id)->avg('value') ?? 0;
        $vacationSpot->save();
    }

    public static function boot(): void
    {
        self::creating(function (self $model) {
            $model->associateWithUser();
            $model->cancelIfAlreadyExist();
            $model->cancelIfWasntFavorite();
        });

        self::created(function (self $model) {
            $model->calculateAverageScore();
        });

        self::updated(function (self $model) {
            $model->calculateAverageScore();
        });

        self::deleted(function (self $model) {
            $model->calculateAverageScore();
        });
        parent::boot();
    }
}
