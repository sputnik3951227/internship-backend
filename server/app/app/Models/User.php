<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Enums\UserRoleEnum;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Table: users.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property string              $email
 * @property string              $password
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null         $first_name
 * @property string|null         $last_name
 * @property string|null         $username
 *
 * === Relationships ===
 * @property Role[]|\Illuminate\Database\Eloquent\Collection         $roles
 * @property Notification[]|\Illuminate\Database\Eloquent\Collection $notifications
 * @property UsersPicture[]|\Illuminate\Database\Eloquent\Collection $pictures
 * @property \Laravel\Sanctum\PersonalAccessToken|null               $tokens
 *
 * === Accessors/Attributes ===
 * @property mixed $password
 */
class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
    ];

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    public function notifications(): HasMany
    {
        return $this->hasMany(Notification::class);
    }

    public function pictures(): HasMany
    {
        return $this->hasMany(UsersPicture::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     */
    public function getJWTIdentifier(): mixed
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Interact with the user's password.
     */
    protected function password(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => Hash::make($value),
        );
    }

    public function sendNotificationsToAdmins(): void
    {
        $admins = Role::query()->where('name', UserRoleEnum::Admin->value)->firstOrFail()->users;
        $notificationMessage = "User #$this->id with email $this->email has been created";
        foreach ($admins as $admin) {
            Notification::query()->create([
                'user_id' => $admin->id,
                'message' => $notificationMessage,
            ]);
        }
    }

    public function attachDefaulRole(): void
    {
        $defaulRole = Role::query()->where('name', UserRoleEnum::User->value)->firstOrFail();
        UsersRole::query()->create([
            'user_id' => $this->id,
            'role_id' => $defaulRole->id,
        ]);
    }

    public static function boot(): void
    {
        self::created(function (self $model) {
            $model->sendNotificationsToAdmins();
            $model->attachDefaulRole();
        });
        parent::boot();
    }

    public function isAdmin(): bool
    {
        return $this->roles()->where('name', UserRoleEnum::Admin->value)->exists();
    }

    public function isBlocked(): bool
    {
        return $this->roles()->where('name', UserRoleEnum::Blocked->value)->exists();
    }
}
