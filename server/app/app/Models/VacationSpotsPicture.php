<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacationSpotsPicture extends Model
{
    use HasFactory;

    protected $fillable = [
        'picture_link',
        'vacation_spot_id',
    ];
}
