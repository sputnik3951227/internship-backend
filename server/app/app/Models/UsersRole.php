<?php

namespace App\Models;

use App\Enums\UserRoleEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Table: users_roles.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property int                 $user_id
 * @property int                 $role_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * === Relationships ===
 * @property User|null $user
 * @property Role|null $role
 */
class UsersRole extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'role_id',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function cancelIfAlreadyExist(): void
    {
        if (
            self::query()
                ->where('user_id', $this->user_id)
                ->where('role_id', $this->role_id)
                ->exists()
        ) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, 'The user already has this role');
        }
    }

    public function createNotificationAboutBlockingStatus($notificationMessage): void
    {
        $user = $this->user;
        $roleName = $this->role->name;
        if ($roleName === UserRoleEnum::Blocked->value) {
            Notification::query()->create([
                'user_id' => $user->id,
                'message' => $notificationMessage,
            ]);
        }
    }

    public static function boot(): void
    {
        self::creating(function (self $model) {
            $model->cancelIfAlreadyExist();
        });

        self::created(function (self $model) {
            $model->createNotificationAboutBlockingStatus('You have been blocked by the administrator');
        });

        self::deleted(function (self $model) {
            $model->createNotificationAboutBlockingStatus('You have been unblocked');
        });

        parent::boot();
    }
}
