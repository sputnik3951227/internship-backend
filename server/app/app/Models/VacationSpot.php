<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Table: vacation_spots.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $description
 * @property string              $latitude
 * @property string              $longitude
 * @property float               $average_score
 * @property string              $country
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int                 $spot_category_id
 */
class VacationSpot extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'latitude',
        'longitude',
        'country',
        'average_score',
        'spot_category_id',
    ];

    public function spotCategory(): BelongsTo
    {
        return $this->belongsTo(VacationSpotsCategory::class, 'spot_category_id');
    }

    public function usersFavorite(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'favorite_spots', 'vacation_spot_id', 'user_id');
    }

    public function createRecommendations(): void
    {
        $spots = self::query()
            ->where('country', $this->country)
            ->with('usersFavorite')
            ->get();

        // получить уникальные id пользователей
        $uniqueUsers = $spots->pluck('usersFavorite')->flatten()->pluck('id')->unique();

        foreach ($uniqueUsers as $userId) {
            RecommendedSpot::query()->create([
                'user_id' => $userId,
                'vacation_spot_id' => $this->id,
            ]);
        }
    }

    public function sendDeletedPlaceNotifications(): void
    {
        $users = $this->usersFavorite;
        $notificationMessage = "Place $this->name from your favorite list has been deleted";
        foreach ($users as $user) {
            Notification::query()->create([
                'user_id' => $user->id,
                'message' => $notificationMessage,
            ]);
        }
    }

    public static function boot(): void
    {
        self::deleting(function (self $model) {
            $model->sendDeletedPlaceNotifications();
        });

        self::created(function (self $model) {
            $model->createRecommendations();
        });

        parent::boot();
    }
}
