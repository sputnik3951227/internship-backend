<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Table: notifications.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property string              $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int                 $user_id
 *
 * === Relationships ===
 * @property User|null $user
 */
class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'message', 'user_id',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
