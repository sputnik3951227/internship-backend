<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Table: vacation_spots_categories.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property string              $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * === Relationships ===
 * @property VacationSpot[]|\Illuminate\Database\Eloquent\Collection $vacationSpots
 */
class VacationSpotsCategory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    public function vacationSpots(): HasMany
    {
        return $this->hasMany(VacationSpot::class, 'spot_category_id');
    }
}
