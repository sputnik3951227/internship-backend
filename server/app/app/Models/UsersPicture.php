<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Table: users_pictures.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property string              $picture_link
 * @property bool                $is_profile_picture
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int                 $user_id
 *
 * === Relationships ===
 * @property User|null $user
 */
class UsersPicture extends Model
{
    use AssociateUserTrait;
    use HasFactory;

    protected $table = 'users_pictures';

    protected $fillable = [
        'picture_link',
        'user_id',
        'is_profile_picture',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    private function checkNewProfilePicture(): void
    {
        if ($this->is_profile_picture) {
            self::where('user_id', $this->user_id)->update(['is_profile_picture' => false]);
        }
    }

    public static function boot(): void
    {
        self::creating(function (self $model) {
            $model->associateWithUser();
            $model->checkNewProfilePicture();
        });
        parent::boot();
    }
}
