<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

trait AssociateUserTrait
{
    private function associateWithUser($userID = null): void
    {
        $this->user_id = $userID ?? Auth::id();
    }
}
