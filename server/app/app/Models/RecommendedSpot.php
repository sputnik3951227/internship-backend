<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Table: recommended_spots.
 *
 * === Columns ===
 *
 * @property int                 $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int                 $vacation_spot_id
 * @property int                 $user_id
 *
 * === Relationships ===
 * @property User|null $user
 */
class RecommendedSpot extends Model
{
    use HasFactory;

    protected $table = 'recommended_spots';

    protected $fillable = [
        'user_id',
        'vacation_spot_id',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
