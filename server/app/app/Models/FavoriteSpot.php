<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FavoriteSpot extends Model
{
    use AssociateUserTrait;
    use HasFactory;

    protected $table = 'favorite_spots';

    protected $fillable = [
        'planned_time',
        'user_id',
        'vacation_spot_id',
    ];

    protected function plannedTime(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => DateTime::createFromFormat('d.m.Y', $value),
        );
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function cancelIfAlreadyExist(): void
    {
        if (
            self::query()
                ->where('user_id', $this->user_id)
                ->where('vacation_spot_id', $this->vacation_spot_id)
                ->exists()
        ) {
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "You've already added this spot to your favorites");
        }
    }

    public static function boot(): void
    {
        self::creating(function (self $model) {
            $model->associateWithUser();
            $model->cancelIfAlreadyExist();
        });
        parent::boot();
    }
}
