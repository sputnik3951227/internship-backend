<?php

namespace Database\Seeders;

use App\Enums\UserRoleEnum;
use App\Models\Role;

class BlockedUserSeeder extends UserSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $blockedRole = Role::query()
            ->where('name', UserRoleEnum::Blocked->value)->firstOrFail();

        $userID = $this->createUserGetId('blocked@mail.com');
        $this->attachRole($userID, $blockedRole->id);
    }
}
