<?php

namespace Database\Seeders;

use App\Enums\UserRoleEnum;
use App\Models\Role;

class AdminUserSeeder extends UserSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRole = Role::query()
            ->where('name', UserRoleEnum::Admin->value)->firstOrFail();

        $userID = $this->createUserGetId('admin@mail.com');
        $this->attachRole($userID, $adminRole->id);
    }
}
