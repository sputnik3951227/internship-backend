<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UsersRole;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    protected function createUserGetId(string $email): int
    {
        $user = User::firstOrCreate([
            'email' => $email,
        ], [
            'password' => 'password',
        ]);

        return $user->id;
    }

    protected function attachRole(int $userID, int $roleID)
    {
        UsersRole::firstOrCreate([
            'user_id' => $userID,
            'role_id' => $roleID,
        ]);
    }
}
