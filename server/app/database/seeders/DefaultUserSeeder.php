<?php

namespace Database\Seeders;

use App\Enums\UserRoleEnum;
use App\Models\Role;

class DefaultUserSeeder extends UserSeeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $defaultRole = Role::query()
            ->where('name', UserRoleEnum::User->value)->firstOrFail();

        $userID = $this->createUserGetId('user@mail.com');
        $this->attachRole($userID, $defaultRole->id);
    }
}
