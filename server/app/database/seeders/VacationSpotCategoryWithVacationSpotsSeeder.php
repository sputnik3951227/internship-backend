<?php

namespace Database\Seeders;

use App\Enums\SpotCategoryExampleEnum;
use App\Models\VacationSpot;
use App\Models\VacationSpotsCategory;
use Illuminate\Database\Seeder;

class VacationSpotCategoryWithVacationSpotsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $spotsPerCategoryCount = 3;
        $categories = SpotCategoryExampleEnum::values();

        foreach ($categories as $category) {
            $vacationSpotsCategory = VacationSpotsCategory::firstOrCreate([
                'name' => $category,
            ]);

            for ($j = 0; $j < $spotsPerCategoryCount; $j++) {
                VacationSpot::factory()->state([
                    'spot_category_id' => $vacationSpotsCategory->id,
                ])->create();
            }
        }
    }
}
