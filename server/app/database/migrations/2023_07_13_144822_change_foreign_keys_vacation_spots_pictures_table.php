<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('vacation_spots_pictures', function (Blueprint $table) {
            $table->dropConstrainedForeignId('vacation_spot_id');
        });

        Schema::table('vacation_spots_pictures', function (Blueprint $table) {
            $table->foreignId('vacation_spot_id')
                ->references('id')
                ->on('vacation_spots')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('vacation_spots_pictures', function (Blueprint $table) {
            $table->dropConstrainedForeignId('vacation_spot_id');
        });

        Schema::table('vacation_spots_pictures', function (Blueprint $table) {
            $table->foreignId('vacation_spot_id')
                ->references('id')
                ->on('vacation_spots');
        });
    }
};
