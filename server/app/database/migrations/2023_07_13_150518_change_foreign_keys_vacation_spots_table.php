<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('vacation_spots', function (Blueprint $table) {
            $table->dropConstrainedForeignId('spot_category_id');
        });

        Schema::table('vacation_spots', function (Blueprint $table) {
            $table->foreignId('spot_category_id')
                ->references('id')
                ->on('vacation_spots_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('vacation_spots', function (Blueprint $table) {
            $table->dropConstrainedForeignId('spot_category_id');
        });

        Schema::table('vacation_spots', function (Blueprint $table) {
            $table->foreignId('spot_category_id')
                ->references('id')
                ->on('vacation_spots_categories');
        });
    }
};
