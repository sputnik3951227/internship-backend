<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_pictures', function (Blueprint $table) {
            $table->id();
            $table->string('picture_link', 255)->unique();
            $table->foreignId('user_id')
                ->references('id')
                ->on('users');
            $table->boolean('is_profile_picture');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users_pictures');
    }
};
