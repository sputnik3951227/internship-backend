<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vacation_spots_pictures', function (Blueprint $table) {
            $table->id();
            $table->string('picture_link', 255)->unique();
            $table->foreignId('vacation_spot_id')
                ->references('id')
                ->on('vacation_spots');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vacation_spots_pictures');
    }
};
