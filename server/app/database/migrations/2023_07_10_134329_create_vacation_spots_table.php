<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vacation_spots', function (Blueprint $table) {
            $table->id();
            $table->string('name', 128)->unique();
            $table->text('description');
            $table->string('latitude', 16);
            $table->string('longitude', 16);
            $table->float('average_score', 2, 1)->default(0.0);
            $table->foreignId('spot_category_id')
                ->references('id')
                ->on('vacation_spots_categories');
            $table->enum('country', [
                'France',
                'Spain',
                'Italy',
                'United States',
                'China',
                'United Kingdom',
                'Germany',
                'Mexico',
                'Thailand',
                'Japan',
                'Russia',
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vacation_spots');
    }
};
