include .env
## help: print this help message
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'

## env: copy .env.example file to .env
.PHONY: env
env:
	cp .env.example .env

## postman: copy collection from current directory to server/collection folder and add timestamp, example: make postman title=your_title_here 
.PHONY: postman
postman:
	mv InternshipBackend.postman_collection.json ./server/collection/$(shell date +%Y_%m_%d_%H%M%S)_$(title).postman_collection.json

## up: build and run the application
.PHONY: up
up:
	docker-compose up -d --build

## down: stop application and delete containers
.PHONY: down
down:
	docker-compose down

## bash: open app console
.PHONY: bash
bash:
	docker exec -it app bash

## restart: restart containers
.PHONY: restart
restart:
	docker-compose restart

## migrate/up: apply migrations
.PHONY: migrate/up
migrate/up:
	docker-compose exec app php artisan migrate

## migrate/reset: reset migrations
.PHONY: migrate/reset
migrate/reset:
	docker-compose exec app php artisan migrate:reset

## migrate/fresh: drop all tables from the database and then execute the migrate command
.PHONY: migrate/fresh
migrate/fresh:
	docker-compose exec app php artisan migrate:fresh

## db/open: open sql console
.PHONY: db/open
db/open:
	docker-compose exec db psql -d ${DB_DATABASE}

## db/seed: seed the database
.PHONY: db/seed
db/seed:
	docker-compose exec app php artisan db:seed